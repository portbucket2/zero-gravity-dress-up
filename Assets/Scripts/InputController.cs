using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] bool inputEnbled = true;
    [SerializeField] Camera mainCam;
    [SerializeField] float slingShotForce;
    [SerializeField] DottedLine dottedLine;

    readonly string FLOATER = "floater";

    [Header("Debug : ")]
    [SerializeField] Transform slingShot;
    Ray ray;
    RaycastHit hit;
    
    Vector3 mouseStart;

    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inputEnbled)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            ray = mainCam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100f)) 
            {
                if (hit.transform.CompareTag(FLOATER))
                {
                    slingShot = hit.transform;
                    mouseStart = Input.mousePosition;

                    gameController.SecondTutorial();
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (slingShot)
            {
                Vector3 dir = mainCam.WorldToScreenPoint(slingShot.position) - Input.mousePosition;
                dir.z = dir.y;
                dir.y = 0f;
                dottedLine.ShowLine(dir, slingShot.position);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (slingShot)
            {
                Vector3 dir = mainCam.WorldToScreenPoint(slingShot.position) - Input.mousePosition;
                float dis = Vector3.Distance(mouseStart, Input.mousePosition);
                dir.z = dir.y;
                dir.y = 0f;
                Rigidbody rb = slingShot.GetComponent<Rigidbody>();
                rb.isKinematic = true;
                rb.isKinematic = false;
                rb.AddForce(dir * slingShotForce * slingShot.GetComponent<Rigidbody>().mass, ForceMode.Impulse);
            }
            slingShot = null;
            dottedLine.DisableLine();
            gameController.ThirdTutorial();
        }
    }

    public void DiableInput() { inputEnbled = false; }
    public void EnableInput() { inputEnbled = true; }
}
