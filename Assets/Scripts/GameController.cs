using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }

    [SerializeField] InputController inputController;
    [SerializeField] Camera mainCamera;
    [SerializeField] CharacterController characterController;
    [SerializeField] CopyAnimation copyAnimation;
    [SerializeField] UIController uIController;
    [SerializeField] ConfigurableJoint centerAnchor;
    [SerializeField] Transform dressHolder;
    [SerializeField] Tutorial tutorial;


    [SerializeField] List<Transform> spawnPositions;

    [Header("Debug: ")]
    [SerializeField] LevelData levelData;
    [SerializeField] int currentLevel = 0;
    [SerializeField] GameDataSheet data;

    GameManager gameManager;
    AnalyticsController analytics;


    void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();
        analytics.LevelStarted();

        data = gameManager.GetDataSheet();
        currentLevel = gameManager.GetlevelCount();
        levelData = new LevelData();
        levelData = data.levelDatas[currentLevel];

        GameObject gg = Instantiate(levelData.characterRig);
        characterController = gg.GetComponent<CharacterController>();


        centerAnchor.connectedBody = characterController.GetHipBone().GetComponent<Rigidbody>();

        FirstTutorial();
    }

    public void EventOne() { Debug.Log("event 1 complete!!"); }
    public void EventTwo() { Debug.Log("event 2 complete!!"); }
    public void EventThree() { Debug.Log("event 3 complete!!"); }

    public UIController GetUI() { return uIController; }
    public InputController GetInputController() { return inputController; }
    public CharacterController GetCharacterController() { return characterController; }
    public Tutorial GetTutorial() { return tutorial; }


    public void LevelComplete()
    {
        uIController.LevelComplete();
        analytics.LevelCompleted();
    }
    public void LevelFailed()
    {
        analytics.LevelFailed();
    }
    public List<Transform> GetSpawnPositions()
    {
        return spawnPositions;
    }
    public int CurrentLevel() { return currentLevel; }
    public GameDataSheet GetData() { return data; }
    public Transform GetDressHolder() { return dressHolder; }

    public void FirstTutorial()
    {
        Debug.Log("tutorial call: " + gameManager.TutorialAlreadySeen());
        if (!gameManager.TutorialAlreadySeen())
        {
            // show tutorial
            tutorial.StartTutorial();
        }
        else
        {
            tutorial.TutorialSeen();
        }
    }
    public void SecondTutorial()
    {
        tutorial.PulledBack();
    }
    public void ThirdTutorial()
    {
        tutorial.Released();
    }

}
