using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tutorial : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text1;
    [SerializeField] TextMeshProUGUI text2;
    [SerializeField] GameObject arrow1;
    [SerializeField] GameObject arrow2;
    [SerializeField] GameObject arrow0;

    readonly string Dial1 = "Drag Back Clothes To Aim";
    readonly string Dial2 = "Release To Shoot";
    readonly string Dial3 = "Match The Dress";

    WaitForSeconds WAIT = new WaitForSeconds(3f);
    bool tutorialComplete = false;
    GameManager gameManager;
    void Start()
    {
        gameManager = GameManager.GetManager();        
    }

    public void StartTutorial()
    {
        if (tutorialComplete)
            return;

        text1.gameObject.SetActive(true);
        text2.gameObject.SetActive(false);

        text1.text = Dial1;
        arrow1.SetActive(true);
        arrow2.SetActive(true);
        arrow0.SetActive(false);
    }
    public void TutorialSeen()
    {
        text1.gameObject.SetActive(false);
        text2.gameObject.SetActive(false);

        arrow1.SetActive(false);
        arrow2.SetActive(false);
        arrow0.SetActive(false);

        tutorialComplete = true;
        if(gameManager)
            gameManager.TutorialSeen();
    }

    public void PulledBack()
    {
        if (tutorialComplete)
            return;

        text1.text = Dial2;
        arrow1.SetActive(false);
        arrow2.SetActive(false);
    }
    public void Released()
    {
        if (tutorialComplete)
            return;

        text1.gameObject.SetActive(false);
        text2.gameObject.SetActive(true);
        text2.text = Dial3;
        arrow0.SetActive(true);
        StartCoroutine(PulledBackRoutine());
    }

    IEnumerator PulledBackRoutine()
    {
        yield return WAIT;
        TutorialSeen();
    }

}
