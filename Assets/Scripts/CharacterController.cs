using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField] float richoForce = 1f;
    [SerializeField] Transform hip;
    [SerializeField] ParticleSystem sparkleParticle;
    [SerializeField] CharacterRig characterRig;

    [Header("Debug :")]
    [SerializeField] List<Rigidbody> ragDollRbs;
    [SerializeField] Rigidbody rb;
    [SerializeField] float maxRagTime = 2f;
    [SerializeField] float randomForce = 10f;
    [SerializeField] LevelData levelData;
    [SerializeField] int currentLevel = 0;
    [SerializeField] GameObject targetTop;
    [SerializeField] GameObject targetBottom;
    [SerializeField] bool isTopDressCorrect = false;
    [SerializeField] bool isBottomDressCorrect = false;
    [SerializeField] int currentRigIndex = 0;
    [SerializeField] List<Transform> spawnPositions;

    GameObject top;
    GameObject bottom;
    GameObject top1;
    GameObject bottom1;
    

    float timeCounter = 0;
    bool isLevelComplete = false;

    readonly string BODY = "body";

    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    GameController gameController;

    private void Awake()
    {
        rb = transform.GetChild(0).GetComponent<Rigidbody>();
    }
    void Start()
    {
        gameController = GameController.GetController();
        currentLevel = gameController.CurrentLevel();
        levelData = new LevelData();
        levelData = gameController.GetData().levelDatas[currentLevel];
        RagDollInit();
    }
    private void Update()
    {
        Vector3 temp = new Vector3(transform.position.x, 1.5f, transform.position.z);
        transform.position = temp;
        //if (isCounting)
        //{
        //    if (timeCounter > 0)
        //    {
        //        timeCounter -= Time.deltaTime;
        //    }
        //    else
        //    {
        //        // count complete
        //        isCounting = false;
        //        // engage animator
        //        animator.enabled = true;
        //    }
        //}
    }
    private void FixedUpdate()
    {
        hip.localEulerAngles = Vector3.zero;
    }
    void RagDollInit()
    {
        ragDollRbs = new List<Rigidbody>();
        ragDollRbs.Add(hip.GetComponent<Rigidbody>());
        hip.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        hip.gameObject.tag = BODY;
        hip.gameObject.layer = 6;
        GetRBsInChildren(hip);

        int max = ragDollRbs.Count;
        for (int i = 0; i < max; i++)
        {
            ragDollRbs[i].useGravity = false;
            //ragDollRbs[i].useGravity = false;
            ragDollRbs[i].gameObject.tag = BODY;
            ragDollRbs[i].gameObject.layer = 6;
            //ragDollRbs[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
            ragDollRbs[i].gameObject.AddComponent<BodyPart>();

            BodyParts part;
            if (ragDollRbs[i].transform.position.z > hip.transform.position.z)
                part = BodyParts.Torso;
            else
                part = BodyParts.Waist;

            ragDollRbs[i].gameObject.GetComponent<BodyPart>().SetBodyPart(this, part);
        }
        RigInit();
    }
    void GetRBsInChildren(Transform _parent)
    {
        int total = _parent.childCount;
        if (total > 0)
        {
            for (int i = 0; i < total; i++)
            {
                if (_parent.GetChild(i).GetComponent<Rigidbody>())
                {
                    ragDollRbs.Add(_parent.GetChild(i).GetComponent<Rigidbody>());                    
                }
                GetRBsInChildren(_parent.GetChild(i));
            }
        }
    }
    void RigInit()
    {
        int max = characterRig.bodies.Count;
        for (int i = 0; i < max; i++)
        {
            characterRig.bodies[i].body.SetActive(false);
        }
        currentRigIndex = levelData.bodyIndex;
        characterRig.bodies[currentRigIndex].body.SetActive(true);
        DressInit();
    }
    void DressInit()
    {
        characterRig.bodies[currentRigIndex].underWearTop.SetActive(true);
        characterRig.bodies[currentRigIndex].underWearBottom.SetActive(true);
        characterRig.bodies[currentRigIndex].set1Top.SetActive(false);
        characterRig.bodies[currentRigIndex].set1Bottom.SetActive(false);
        characterRig.bodies[currentRigIndex].set2Top.SetActive(false);
        characterRig.bodies[currentRigIndex].set2Bottom.SetActive(false);

        if (levelData.dressIndex1 == 0)
        {
            targetTop = characterRig.bodies[currentRigIndex].set1Top;
            targetBottom = characterRig.bodies[currentRigIndex].set1Bottom;
        }
        else
        {
            targetTop = characterRig.bodies[currentRigIndex].set2Top;
            targetBottom = characterRig.bodies[currentRigIndex].set2Bottom;
        }

        bool topIsTrue = false;
        bool bottomIsTrue = false;
        if (Random.Range(0f, 1f) > 0.5f)
        {
            topIsTrue = true;
            bottomIsTrue = false;
        }
        else
        {
            topIsTrue = false;
            bottomIsTrue = true;
        }
        spawnPositions = gameController.GetSpawnPositions();

        top = Instantiate(levelData.topPrefab, spawnPositions[0].position, Quaternion.identity);
        top.GetComponent<Floater>().SetFloater(levelData.floaterIndex1, levelData.dressIndex1, BodyParts.Torso, topIsTrue);

        bottom = Instantiate(levelData.bottomPrefab, spawnPositions[1].position, Quaternion.identity);
        bottom.GetComponent<Floater>().SetFloater(levelData.floaterIndex1, levelData.dressIndex1, BodyParts.Waist, topIsTrue);

        top1 = Instantiate(levelData.topPrefab, spawnPositions[2].position, Quaternion.identity);
        top1.GetComponent<Floater>().SetFloater(levelData.floaterIndex2, levelData.dressIndex2, BodyParts.Torso, bottomIsTrue);

        bottom1 = Instantiate(levelData.bottomPrefab, spawnPositions[3].position, Quaternion.identity);
        bottom1.GetComponent<Floater>().SetFloater(levelData.floaterIndex2, levelData.dressIndex2, BodyParts.Waist, bottomIsTrue);

        Vector3 offset = new Vector3(0f, 0f, 0.5f);
        Transform tt = gameController.GetDressHolder();
        GameObject topPreview = Instantiate(levelData.topPrefab, tt.position + offset, Quaternion.identity);
        GameObject bottomPreview = Instantiate(levelData.bottomPrefab, tt.position - offset, Quaternion.identity);
        if (topIsTrue)
        {
            topPreview.GetComponent<Floater>().SetFloater(levelData.floaterIndex1, levelData.dressIndex1, BodyParts.Torso, topIsTrue);
            bottomPreview.GetComponent<Floater>().SetFloater(levelData.floaterIndex1, levelData.dressIndex1, BodyParts.Waist, topIsTrue);
        }
        else
        {
            topPreview.GetComponent<Floater>().SetFloater(levelData.floaterIndex2, levelData.dressIndex2, BodyParts.Torso, bottomIsTrue);
            bottomPreview.GetComponent<Floater>().SetFloater(levelData.floaterIndex2, levelData.dressIndex2, BodyParts.Waist, bottomIsTrue);
        }
    }
    void LevelCompleteCheck()
    {
        if (isTopDressCorrect && isBottomDressCorrect)
        {
            // level complete
            isLevelComplete = true;
            gameController.GetInputController().DiableInput();
            StartCoroutine(LevelCompleteRoutine());
        }
    }
    IEnumerator LevelCompleteRoutine()
    {
        yield return WAITONE;
        gameController.LevelComplete();
    }

    

    public Transform GetHipBone() { return hip; }

    public List<Rigidbody> GetRigidBodies() { return ragDollRbs; }
    public void AddForce(float force, Vector3 dir)
    {
        rb.AddForce(dir * force * richoForce, ForceMode.Impulse);
    }

    public void FloaterHit()
    {
        //animator.enabled = false;
        timeCounter = maxRagTime;
    }

    public void TopDressCorrect() { isTopDressCorrect = true; LevelCompleteCheck(); }
    public void TopDressWrong() { isTopDressCorrect = false; LevelCompleteCheck(); }
    public void BottomDressCorrect() { isBottomDressCorrect = true; LevelCompleteCheck(); }
    public void BottomDressWrong() { isBottomDressCorrect = false; LevelCompleteCheck(); }

    public void ActivateTopDress(int _dressIndex)
    {
        if (isLevelComplete)
            return;
        characterRig.bodies[currentRigIndex].underWearTop.SetActive(false);
        if (_dressIndex == 0)
        {
            characterRig.bodies[currentRigIndex].set1Top.SetActive(true);
            characterRig.bodies[currentRigIndex].set2Top.SetActive(false);
            top.SetActive(false);
            top.GetComponent<Floater>().ResetPosition();
            top1.SetActive(true);
        }
        else
        {
            characterRig.bodies[currentRigIndex].set2Top.SetActive(true);
            characterRig.bodies[currentRigIndex].set1Top.SetActive(false);
            top.SetActive(true);
            top1.SetActive(false);
            top1.GetComponent<Floater>().ResetPosition();
        }
        sparkleParticle.Play();
    }
    public void ActivateBottomDress(int _dressIndex)
    {
        if (isLevelComplete)
            return;
        characterRig.bodies[currentRigIndex].underWearBottom.SetActive(false);
        if (_dressIndex == 0)
        {
            characterRig.bodies[currentRigIndex].set1Bottom.SetActive(true);
            characterRig.bodies[currentRigIndex].set2Bottom.SetActive(false);
            bottom.SetActive(false);
            bottom.GetComponent<Floater>().ResetPosition();
            bottom1.SetActive(true);
        }
        else
        {
            characterRig.bodies[currentRigIndex].set2Bottom.SetActive(true);
            characterRig.bodies[currentRigIndex].set1Bottom.SetActive(false);
            bottom.SetActive(true);
            bottom1.SetActive(false);
            bottom1.GetComponent<Floater>().ResetPosition();
        }
        sparkleParticle.Play();
    }
    public void StartingForce()
    {
        Vector2 dd = Random.insideUnitCircle;
        Vector3 dir = new Vector3(dd.x, 0f, dd.y);
        top.GetComponent<Rigidbody>().AddForce(dir * randomForce);
        bottom.GetComponent<Rigidbody>().AddForce(dir * randomForce);
        top1.GetComponent<Rigidbody>().AddForce(dir * randomForce);
        bottom1.GetComponent<Rigidbody>().AddForce(dir * randomForce);
    }

    

}
[System.Serializable]
public struct CharacterRig
{
    public List<CharatcerBody> bodies;
}

[System.Serializable]
public struct CharatcerBody
{
    public GameObject body;
    public GameObject underWearTop;
    public GameObject underWearBottom;
    public GameObject set1Top;
    public GameObject set1Bottom;
    public GameObject set2Top;
    public GameObject set2Bottom;
}

