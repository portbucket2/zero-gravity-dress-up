﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LionStudios.Suite.Analytics;
using LionStudios.Debugging;
//using com.faithstudio.SDK;
using GameAnalyticsSDK;

public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController analyticsController;

    int levelcount = 1;

    GameManager gameManager;
    private void Awake()
    {
        levelcount = 1;
        analyticsController = this;
        gameManager = transform.GetComponent<GameManager>();
    }
    private void Start()
    {
        levelcount = gameManager.GetDataManager().GetGamePlayer.levelsCompleted;
        GameAnalytics.Initialize();
        LionAnalytics.GameStart();
    }

    public static AnalyticsController GetController()
    {
        return analyticsController;
    }

    public void LevelStarted()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Level " + levelcount);
        //SaveGame();
        LionDebugger.Hide();
        LionAnalytics.LevelStart(levelcount);
    }
    public void LevelCompleted()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Level " + levelcount);
        LionAnalytics.LevelComplete(levelcount);
        levelcount++;
        SaveGame();
    }
    public void LevelFailed()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "World01", "Level " + levelcount);
        LionAnalytics.LevelFail(levelcount);
    }
    public void SaveGame()
    {
        GamePlayer gp = new GamePlayer();
        gp.name = "";
        gp.id = 1;
        gp.levelsCompleted = levelcount;
        gp.totalCoins = 0;// GameController.GetController().GetTotalCoins();
        gp.lastPlayedLevel = gameManager.GetlevelCount();
        gp.handTutorialShown = true;

        gameManager.GetDataManager().SetGameplayerData(gp);
    }

}
