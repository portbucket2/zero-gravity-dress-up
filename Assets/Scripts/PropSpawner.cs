using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSpawner : MonoBehaviour
{
    [SerializeField] int propCountMin;
    [SerializeField] int propCountMax;
    [SerializeField] float spawnRadius;
    [SerializeField] float initialForce;
    [SerializeField] List<GameObject> props;
    void Start()
    {
        int propsCount = props.Count - 1;
        int randCount = Random.Range(0, propsCount);
        int max = Random.Range(propCountMin, propCountMax);
        for (int i = 0; i < max; i++)
        {
            Vector2 circle = Random.insideUnitCircle * spawnRadius;
            Vector3 pos = new Vector3(circle.x, 1.5f, circle.y);
            GameObject gg = Instantiate(props[randCount], pos, props[randCount].transform.rotation);
            Vector3 dir = gg.transform.position - transform.position;
            gg.GetComponent<Rigidbody>().AddForce(dir * initialForce);

            randCount++;
            if (randCount > propsCount)
                randCount = 0;
        }
    }

}
