﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameDataSheet", order = 1)]
public class GameDataSheet : ScriptableObject
{
    public List<LevelData> levelDatas;

    private void Awake()
    {
               
    }

    
}


[System.Serializable]
public struct LevelData
{
    public GameObject characterRig;
    public GameObject topPrefab;
    public GameObject bottomPrefab;
    public int bodyIndex;
    public int floaterIndex1;
    public int floaterIndex2;
    public int dressIndex1;
    public int dressIndex2;
    
}