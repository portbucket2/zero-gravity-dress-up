using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyAnimation : MonoBehaviour
{
    [SerializeField] bool copy = false;
    [SerializeField] CharacterController characterController;
    [SerializeField] Transform hip;
    [Header("Configurable Joints :")]
    [SerializeField] float springPosition;
    [SerializeField] float springDamp;

    [Header("Debug :")]
    [SerializeField] List<Transform> joints;
    [SerializeField] List<ConfigurableJoint> targetJoints;
    [SerializeField] List<Quaternion> startingRotaions;

    bool startCopy = false;
    int maxJoints = 0;
    Vector3 temp;
    // Start is called before the first frame update
    GameController gameController;
    void Start()
    {
        gameController = GameController.GetController();
        
        Invoke("RagDollInit", 0.1f);
    }

    private void FixedUpdate()
    {
        if (copy && startCopy)
        {
            for (int i = 0; i < maxJoints; i++)
            {
                SetJointRotaion(targetJoints[i], joints[i], startingRotaions[i]);
            }
            temp = targetJoints[0].transform.localPosition;
            //targetJoints[0].transform.localPosition = new Vector3(0f, 1.5f, 0f);
        }
        
    }

    void RagDollInit()
    {
        characterController = gameController.GetCharacterController();

        startCopy = true;
        
        joints = new List<Transform>();
        joints.Add(hip.GetComponent<Transform>());
        hip.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        GetJointsInChildren(hip);

        List<Rigidbody> rbs = new List<Rigidbody>();
        targetJoints = new List<ConfigurableJoint>();
        rbs = characterController.GetRigidBodies();
        startingRotaions = new List<Quaternion>();
        int max = rbs.Count;
        maxJoints = max;
        for (int i = 0; i < max; i++)
        {
            startingRotaions.Add(rbs[i].transform.rotation);
            targetJoints.Add(rbs[i].transform.GetComponent<ConfigurableJoint>());
        }
        SetConfigurableJointsValues();
        gameController.GetCharacterController().StartingForce();
    }

    void GetJointsInChildren(Transform _parent)
    {
        int total = _parent.childCount;
        if (total > 0)
        {
            for (int i = 0; i < total; i++)
            {
                if (_parent.GetChild(i).GetComponent<Rigidbody>())
                {
                    joints.Add(_parent.GetChild(i).GetComponent<Transform>());
                }
                GetJointsInChildren(_parent.GetChild(i));
            }
        }
    }

    void SetJointRotaion(ConfigurableJoint mainBodyJoints, Transform _target, Quaternion _startingRotation)
    {
        mainBodyJoints.SetTargetRotationLocal(_target.rotation, _startingRotation);
    }
    void SetConfigurableJointsValues()
    {
        Debug.Log("setting join values ");
        int max = targetJoints.Count;
        for (int i = 0; i < max; i++)
        {
            JointDrive driveX = targetJoints[i].angularXDrive;
            driveX.positionSpring = springPosition;
            driveX.positionDamper = springDamp;

            JointDrive driveYZ = targetJoints[i].angularYZDrive;
            driveYZ.positionSpring = springPosition;
            driveYZ.positionDamper = springDamp;


            targetJoints[i].angularXDrive = driveX;
            targetJoints[i].angularYZDrive = driveYZ;
        }
    }
}
