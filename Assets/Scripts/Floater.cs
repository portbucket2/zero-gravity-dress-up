using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floater : MonoBehaviour
{

    [SerializeField] BodyParts bodyPartName;
    [SerializeField] bool isMain = false;
    [SerializeField] int index = 0;
    [SerializeField] int dressIndex = 0;
    [SerializeField] List<GameObject> tops;

    Vector3 startPosition;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        startPosition = transform.position;
    }

    public void SetFloater(int _index,int _bodyIndex, BodyParts _part, bool _isMain)
    {
        index = _index;
        dressIndex = _bodyIndex;
        bodyPartName = _part;
        isMain = _isMain;

        int max = tops.Count;
        if (index < max)
        {
            for (int i = 0; i < max; i++)
            {
                tops[i].SetActive(false);
            }
            tops[index].SetActive(true);
        }
    }

    public bool IsMain() { return isMain; }
    public BodyParts BodyPartName() { return bodyPartName; }
    public int DressIndex() { return dressIndex; }

    public void ResetPosition() { transform.position = startPosition; rb.isKinematic = true; rb.isKinematic = false; }
}
