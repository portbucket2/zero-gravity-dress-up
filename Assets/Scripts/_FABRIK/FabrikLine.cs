using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FabrikLine : MonoBehaviour
{
    [SerializeField] Transform tTarget;
    [SerializeField] Transform[] tPoints;

    Vector3 mTarget;
    Vector3[] mPoints;

    LineRenderer ln;

    void Start()
    {
        ln = GetComponent<LineRenderer>();
        mTarget = tTarget.position;
        mPoints = new Vector3[tPoints.Length];
        for (int i = 0; i < tPoints.Length - 1; i++)
        {
            mPoints[i] = tPoints[i].position;
        }
    }

    private void Update()
    {
        Solve(mPoints, mTarget);
        ShowLine();
    }


    const int maxIterations = 100;
    const float minAcceptableDst = 0.05f;

    public void Solve(Vector3[] points, Vector3 target)
    {
        Vector3 origin = points[0];
        float[] segmentLengths = new float[points.Length - 1];
        for (int i = 0; i < points.Length - 1; i++)
        {
            segmentLengths[i] = (points[i + 1] - points[i]).magnitude;
        }

        for (int iteration = 0; iteration < maxIterations; iteration++)
        {
            bool startingFromTarget = iteration % 2 == 0;

            System.Array.Reverse(points);
            System.Array.Reverse(segmentLengths);
            points[0] = (startingFromTarget) ? target : origin;

            for (int i = 1; i < points.Length; i++)
            {
                Vector3 dir = (points[i] - points[i - 1]).normalized;
                points[i] = points[i - 1] + dir * segmentLengths[i - 1];
            }


            int mm = tPoints.Length;
            for (int i = 0; i < mm; i++)
            {
                tPoints[i].position = points[i];
            }

            Debug.Log("working....");
            float dstTotarget = (points[points.Length - 1] - target).magnitude;
            if (!startingFromTarget && dstTotarget <= minAcceptableDst)
            {
                return;
            }
        }
        
    }

    void ShowLine()
    {
        int max = tPoints.Length;
        for (int i = 0; i < max; i++)
        {
            ln.SetPosition(i, tPoints[i].position);
        }
    }
}
