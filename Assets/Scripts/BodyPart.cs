using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    [SerializeField] BodyParts bodyPartName;
    CapsuleCollider col;
    CharacterController characterController;

    readonly string WALL = "wall";
    readonly string BODY = "body";
    readonly string FLOATER = "floater";

    Floater currentFloater;

    private void Awake()
    {
        col = GetComponent<CapsuleCollider>();
        //characterController = transform.parent.GetComponent<CharacterController>();

    }
    void Start()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.contactCount > 0)
        {
            if (!collision.transform.CompareTag(BODY))
            {
                Debug.Log("collided with " + bodyPartName);
                ContactPoint hitPoint = collision.GetContact(0);
                if (collision.transform.CompareTag(WALL))
                {
                    characterController.AddForce(collision.impulse.magnitude * 2f, hitPoint.normal);
                    //Debug.Log("hit wall ");
                }
                if (collision.transform.CompareTag(FLOATER))
                {
                    //characterController.FloaterHit();
                    //Debug.Log("floater hit body part: " + bodyPartName);
                    if (collision.transform.GetComponent<Floater>())
                    {
                        currentFloater = collision.transform.GetComponent<Floater>();
                        if (currentFloater.BodyPartName() == bodyPartName)
                        {
                            Debug.Log("floater hit correct body part: " + bodyPartName);

                            if (bodyPartName == BodyParts.Torso)
                            {
                                characterController.ActivateTopDress(currentFloater.DressIndex());
                                if (currentFloater.IsMain())
                                {
                                    characterController.TopDressCorrect();
                                }
                                else
                                {
                                    characterController.TopDressWrong();
                                }                                    
                            }
                            else
                            {
                                characterController.ActivateBottomDress(currentFloater.DressIndex());
                                if (currentFloater.IsMain())
                                {
                                    characterController.BottomDressCorrect();
                                }
                                else
                                {
                                    characterController.BottomDressWrong();
                                }
                            }

                        }
                    }

                }
            }
            
        }
    }
    public void SetBodyPart(CharacterController character, BodyParts part)
    {
        characterController = character;
        bodyPartName = part;
    }
}

public enum BodyParts
{
    Torso,
    Waist
}
